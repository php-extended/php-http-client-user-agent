<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-user-agent library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\UserAgent\BasicUserAgentProvider;
use PhpExtended\UserAgent\UserAgentProviderInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * UserAgentClient class file.
 * 
 * This class is an implementation of a client which adds user-agent headers
 * to incoming requests.
 * 
 * @author Anastaszor
 */
class UserAgentClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The provider.
	 * 
	 * @var UserAgentProviderInterface
	 */
	protected UserAgentProviderInterface $_provider;
	
	/**
	 * Builds a new UserAgentClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param ?UserAgentProviderInterface $provider
	 */
	public function __construct(ClientInterface $client, ?UserAgentProviderInterface $provider = null)
	{
		$this->_client = $client;
		$this->_provider = $provider ?? new BasicUserAgentProvider();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('User-Agent'))
		{
			$userAgent = $this->_provider->getNextUserAgent();
			if(null !== $userAgent)
			{
				try
				{
					$request = $request->withHeader('User-Agent', $userAgent->getHeaderValue());
				}
				// @codeCoverageIgnoreStart
				catch(InvalidArgumentException $e)
				// @CodeCoverageIgnoreEnd
				{
					// nothing to do
				}
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
