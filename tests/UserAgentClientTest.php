<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-user-agent library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\UserAgentClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PhpExtended\UserAgent\UserAgentInterface;
use PhpExtended\UserAgent\UserAgentProviderInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * UserAgentClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\UserAgentClient
 *
 * @internal
 *
 * @small
 */
class UserAgentClientTest extends TestCase
{
	
	/**
	 * The client to help.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The object to test.
	 * 
	 * @var UserAgentClient
	 */
	protected UserAgentClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$this->_object->sendRequest(new Request());
		
		$this->assertEquals('MY USER AGENT', $this->_client->request->getHeaderLine('User-Agent'));
	}
	
	public function testDoesNotOverwrite() : void
	{
		$request = new Request();
		$request = $request->withHeader('User-Agent', 'FAKE USER AGENT');
		
		$this->_object->sendRequest($request);
		
		$this->assertEquals('FAKE USER AGENT', $this->_client->request->getHeaderLine('User-Agent'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_client = new class() implements ClientInterface
		{
			public $request;
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$this->request = $request;
				
				return new Response();
			}
		};
		
		$mockedUserAgent = $this->getMockForAbstractClass(UserAgentInterface::class);
		$mockedUserAgent->expects($this->any())
			->method('getHeaderValue')
			->willReturn('MY USER AGENT')
		;
		
		$provider = $this->getMockForAbstractClass(UserAgentProviderInterface::class);
		$provider->expects($this->any())
			->method('getNextUserAgent')
			->willReturn($mockedUserAgent)
		;
		
		$this->_object = new UserAgentClient($this->_client, $provider);
	}
	
}
